#pragma once
#include "dllDeclspecs.h"
#include "pch.h"

#ifdef NDEBUG
	const std::vector<const char*> VALIDATION_LAYERS = {};
#else
	#define VALIDATION_LAYERS_ENABLED = true;
	const std::vector<const char*> VALIDATION_LAYERS = {
		"VK_LAYER_KHRONOS_validation"
	};
#endif //NDEBUG

namespace MessengerFunctions
{
	void DestroyDebugUtilsMessengerEXT( VkInstance& instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator );
	VkResult  CreateDebugUtilsMessengerEXT( VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger );
	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback( VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData );
	void PopulateDebugMessengerCreateInfo( VkDebugUtilsMessengerCreateInfoEXT& createInfo );
}

class LavaValidator
{
public:
	EXPORT LavaValidator( );
	EXPORT ~LavaValidator();
	
	EXPORT void SetupMessenger( VkInstance instance );

	template< typename T >
	EXPORT void Attach( T& createInfo , std::vector<const char*> &extensions )
	{
#ifdef VALIDATION_LAYERS_ENABLED
		
		extensions.push_back( VK_EXT_DEBUG_UTILS_EXTENSION_NAME );
#endif
		createInfo.enabledExtensionCount = static_cast<uint32_t>( extensions.size() );
		createInfo.ppEnabledExtensionNames = extensions.data();
#ifdef VALIDATION_LAYERS_ENABLED
		

		if( m_debugCreateInfo.pfnUserCallback == nullptr )
		{
			MessengerFunctions::PopulateDebugMessengerCreateInfo( m_debugCreateInfo );
		}
		createInfo.pNext = ( VkDebugUtilsMessengerCreateInfoEXT*) & m_debugCreateInfo;

		createInfo.enabledLayerCount = static_cast<uint32_t>( VALIDATION_LAYERS.size() );
		createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
#else
		createInfo.enabledLayerCount = 0;
		createInfo.pNext = nullptr;
#endif
	}

	template< typename T >
	EXPORT void AttachToValidationLayer( T& createInfo )
	{
#ifdef VALIDATION_LAYERS_ENABLED
		createInfo.enabledLayerCount = static_cast<uint32_t>( VALIDATION_LAYERS.size() );
		createInfo.ppEnabledLayerNames = VALIDATION_LAYERS.data();
#else
		createInfo.enabledLayerCount = 0;
#endif#
	}

	EXPORT void AssertLayers();
	EXPORT void CleanUp( VkInstance instance );
private:
	VkDebugUtilsMessengerEXT m_debugMessenger;	
	VkDebugUtilsMessengerCreateInfoEXT m_debugCreateInfo;
};

