#pragma once
#include "dllDeclspecs.h"
#include "Rendering/lavaRenderingUtils.h"
#include "Rendering/lavaRenderable.h"
#include "lavaRenderContext.h"

const int MAX_FRAMES_IN_FLIGHT = 2;


namespace SwapChainConstants
{
	const std::vector<const char*> EXTENSIONS = {
		VK_KHR_SWAPCHAIN_EXTENSION_NAME
	};
};

struct SwapChainSupport
{
	VkSurfaceCapabilitiesKHR capabilities = {};
	std::vector<VkSurfaceFormatKHR> formats;
	std::vector<VkPresentModeKHR> presentModes;

	VkSurfaceFormatKHR currentFormat = {};
	VkPresentModeKHR currentPresentMode = VK_PRESENT_MODE_FIFO_KHR;
	VkExtent2D currentExtent = {};
};

class LavaSwapChain
{
public:
	EXPORT LavaSwapChain();
	EXPORT ~LavaSwapChain();
	EXPORT void CleanUp();
	EXPORT void Recreate();
	EXPORT static bool DeviceSupportsExtensions( VkPhysicalDevice physicalDevice );
	EXPORT static bool QueryDeviceSwapChainSupport( RenderContext::RenderContext& context, SwapChainSupport& support );

	EXPORT void AddRenderable( LavaRenderable* renderable );
	EXPORT void RemoveRenderable( LavaRenderable* renderable );

	EXPORT void Render();

private:
	void SetupSwapChain();
	void SetupSwapChainViews();
	void SetupBackBufferRenderPass();
	void SetupFrameBuffers();
	void SetupCommandPool();
	void SetupCommandBuffers();
	void SetupSync();

	SwapChainSupport m_swapChainSupport;
	VkSwapchainKHR m_swapChain;
	VkRenderPass m_backBufferRenderPass;

	std::vector<VkImage> m_swapChainImages;
	std::vector<VkImageView> m_swapChainImageViews;
	std::vector<VkFramebuffer> m_frameBuffers;
	std::vector<VkCommandBuffer> m_commandBuffers;

	std::vector<VkSemaphore> m_imageAvailableSemaphores;
	std::vector<VkSemaphore> m_renderFinishedSemaphores;
	std::vector<VkFence> m_inFlightFences;

	VkCommandPool m_commandPool;

	std::vector<LavaRenderable*> m_renderables;
	uint32_t m_currentFrame;
	bool m_recreateCommandBuffers;
};
