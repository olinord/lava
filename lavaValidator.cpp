#include "pch.h"
#include "lavaValidator.h"
#include <cassert>

namespace MessengerFunctions
{
	void DestroyDebugUtilsMessengerEXT( VkInstance &instance, VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator )
	{
		auto func = ( PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr( instance, "vkDestroyDebugUtilsMessengerEXT" );
		if( func != nullptr )
		{
			func( instance, debugMessenger, pAllocator );
		}
	}

	VkResult CreateDebugUtilsMessengerEXT( VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pDebugMessenger )
	{
		auto func = ( PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr( instance, "vkCreateDebugUtilsMessengerEXT" );
		if( func != nullptr )
		{
			return func( instance, pCreateInfo, pAllocator, pDebugMessenger );
		}
		else
		{
			return VK_ERROR_EXTENSION_NOT_PRESENT;
		}
	}

	static VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback( VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData )
	{
		std::cerr << "validation layer: " << pCallbackData->pMessage << std::endl;
		return VK_FALSE;
	}

	void PopulateDebugMessengerCreateInfo( VkDebugUtilsMessengerCreateInfoEXT& createInfo )
	{
		createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		createInfo.pfnUserCallback = DebugCallback;
	}

}

LavaValidator::LavaValidator() :
	m_debugMessenger(nullptr)
{
	m_debugCreateInfo = {};
}

LavaValidator::~LavaValidator()
{
}

void LavaValidator::SetupMessenger( VkInstance instance )
{
#ifdef VALIDATION_LAYERS_ENABLED
	VkDebugUtilsMessengerCreateInfoEXT createInfo;
	MessengerFunctions::PopulateDebugMessengerCreateInfo( createInfo );
	if( MessengerFunctions::CreateDebugUtilsMessengerEXT( instance, &createInfo, nullptr, &m_debugMessenger ) != VK_SUCCESS )
	{
		throw std::runtime_error( "Failed to set up debug messenger!" );
	}
#endif
}

void LavaValidator::AssertLayers()
{
#ifdef VALIDATION_LAYERS_ENABLED
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties( &layerCount, nullptr );

	std::vector<VkLayerProperties> availableLayers( layerCount );
	vkEnumerateInstanceLayerProperties( &layerCount, availableLayers.data() );

	for( const char* layerName : VALIDATION_LAYERS )
	{
		bool layerFound = false;

		for( const auto& layerProperties : availableLayers )
		{
			if( strcmp( layerName, layerProperties.layerName ) == 0 )
			{
				layerFound = true;
				break;
			}
		}
		if( !layerFound )
		{
			throw std::runtime_error( "Validation layer " + std::string( layerName ) + " requested, but not available!" );
		}
	}
#endif
}

void LavaValidator::CleanUp( VkInstance instance )
{
#ifdef VALIDATION_LAYERS_ENABLED
	MessengerFunctions::DestroyDebugUtilsMessengerEXT( instance, m_debugMessenger, nullptr );
#endif
}
