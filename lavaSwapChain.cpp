#include "pch.h"
#include "lavaSwapChain.h"
#include "imageUtils.h"
#include "Rendering/lavaRenderingUtils.h"
#include "Rendering/lavaRenderable.h"


LavaSwapChain::LavaSwapChain() :
	m_currentFrame( 0 )
{
	SetupSwapChain();
	SetupSwapChainViews();
	SetupBackBufferRenderPass();
	SetupFrameBuffers();
	SetupCommandPool();
	SetupCommandBuffers();
	SetupSync();
}

LavaSwapChain::~LavaSwapChain()
{
	m_swapChainImages.clear();
	m_swapChainImageViews.clear();
}

void LavaSwapChain::Recreate()
{
	int width = 0, height = 0;
	while( width == 0 || height == 0 )
	{
		glfwGetFramebufferSize( RenderContext::appRenderContext.window, &width, &height );
		glfwWaitEvents();
	}
	Logging::Info( "Recreating swapchain" );

	vkDeviceWaitIdle( RenderContext::appRenderContext.logicalDevice);

	for( size_t i = 0; i < m_frameBuffers.size(); i++ )
	{
		vkDestroyFramebuffer( RenderContext::appRenderContext.logicalDevice, m_frameBuffers[i], nullptr );
	}

	vkFreeCommandBuffers( 
		RenderContext::appRenderContext.logicalDevice, 
		m_commandPool, 
		static_cast<uint32_t>( m_commandBuffers.size() ), 
		m_commandBuffers.data() 
	);
		
	for( auto renderable : m_renderables )
	{
		renderable->CleanUp();
	}

	vkDestroyRenderPass( RenderContext::appRenderContext.logicalDevice, m_backBufferRenderPass, nullptr );

	for( size_t i = 0; i < m_swapChainImageViews.size(); i++ )
	{
		vkDestroyImageView( RenderContext::appRenderContext.logicalDevice, m_swapChainImageViews[i], nullptr );
	}

	vkDestroySwapchainKHR( RenderContext::appRenderContext.logicalDevice, m_swapChain, nullptr );

	SetupSwapChain();
	SetupSwapChainViews();
	SetupBackBufferRenderPass();
	for( auto renderable : m_renderables )
	{
		renderable->Finalize( m_backBufferRenderPass );
	}
	SetupFrameBuffers();
	SetupCommandBuffers();
	Logging::Info( "Swapchain successfully recreated" );
}

void LavaSwapChain::CleanUp()
{
	for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++ )
	{
		vkDestroySemaphore( RenderContext::appRenderContext.logicalDevice, m_renderFinishedSemaphores[i], nullptr );
		vkDestroySemaphore( RenderContext::appRenderContext.logicalDevice, m_imageAvailableSemaphores[i], nullptr );
		vkDestroyFence( RenderContext::appRenderContext.logicalDevice, m_inFlightFences[i], nullptr );
	}
	
	vkDestroyCommandPool( RenderContext::appRenderContext.logicalDevice, m_commandPool, nullptr );
	for( auto framebuffer : m_frameBuffers )
	{
		vkDestroyFramebuffer( RenderContext::appRenderContext.logicalDevice, framebuffer, nullptr );
	}

	for( auto renderable : m_renderables )
	{
		renderable->CleanUp();
	}

	vkDestroyRenderPass( RenderContext::appRenderContext.logicalDevice, m_backBufferRenderPass, nullptr );
	for( auto imageView : m_swapChainImageViews )
	{
		vkDestroyImageView( RenderContext::appRenderContext.logicalDevice, imageView, nullptr );
	}
	vkDestroySwapchainKHR( RenderContext::appRenderContext.logicalDevice, m_swapChain, nullptr );
}

void LavaSwapChain::SetupSwapChain()
{
	QueryDeviceSwapChainSupport( RenderContext::appRenderContext, m_swapChainSupport );

	uint32_t imageCount = m_swapChainSupport.capabilities.minImageCount + 1;
	if( m_swapChainSupport.capabilities.maxImageCount > 0 && imageCount > m_swapChainSupport.capabilities.maxImageCount )
	{
		imageCount = m_swapChainSupport.capabilities.maxImageCount;
	}

	VkSwapchainCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	createInfo.surface = RenderContext::appRenderContext.surface;

	createInfo.minImageCount = imageCount;
	createInfo.imageFormat = m_swapChainSupport.currentFormat.format;
	createInfo.imageColorSpace = m_swapChainSupport.currentFormat.colorSpace;
	createInfo.imageExtent = m_swapChainSupport.currentExtent;
	createInfo.imageArrayLayers = 1;
	createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

	uint32_t graphicsFamily = RenderContext::appRenderContext.graphicsFamily;
	uint32_t presentFamily = RenderContext::appRenderContext.presentFamily;

	if( graphicsFamily != presentFamily )
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		uint32_t queueFamilyIndices[] = { graphicsFamily, presentFamily };

		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else
	{
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	}

	createInfo.preTransform = m_swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = m_swapChainSupport.currentPresentMode;
	createInfo.clipped = VK_TRUE;

	createInfo.oldSwapchain = VK_NULL_HANDLE;

	if( vkCreateSwapchainKHR( RenderContext::appRenderContext.logicalDevice, &createInfo, nullptr, &m_swapChain ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create swap chain!" );
	}

	vkGetSwapchainImagesKHR( RenderContext::appRenderContext.logicalDevice, m_swapChain, &imageCount, nullptr );
	m_swapChainImages.resize( imageCount );
	vkGetSwapchainImagesKHR( RenderContext::appRenderContext.logicalDevice, m_swapChain, &imageCount, m_swapChainImages.data() );

	RenderContext::appRenderContext.extent = m_swapChainSupport.currentExtent;
}


void LavaSwapChain::SetupSwapChainViews()
{
	m_swapChainImageViews.resize( m_swapChainImages.size() );

	for( size_t i = 0; i < m_swapChainImages.size(); i++ )
	{
		auto result = ImageUtils::CreateImageViewFromImage( &m_swapChainImages[i], &m_swapChainSupport.currentFormat.format, RenderContext::appRenderContext.logicalDevice );
		if( !result.ok )
		{
			throw std::runtime_error( "failed to create swap chain image view" );
		}

		m_swapChainImageViews[i] = result.value;
	}
}

void LavaSwapChain::SetupBackBufferRenderPass()
{
	VkAttachmentDescription colorAttachment = {};
	colorAttachment.format = m_swapChainSupport.currentFormat.format;
	colorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	colorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	colorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	colorAttachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
	colorAttachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
	colorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	colorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference colorAttachmentRef = {};
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

	VkSubpassDescription subpass = {};
	subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;

	VkRenderPassCreateInfo renderPassInfo = {};
	renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	renderPassInfo.attachmentCount = 1;
	renderPassInfo.pAttachments = &colorAttachment;
	renderPassInfo.subpassCount = 1;
	renderPassInfo.pSubpasses = &subpass;

	if( vkCreateRenderPass( RenderContext::appRenderContext.logicalDevice, &renderPassInfo, nullptr, &m_backBufferRenderPass ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create render pass!" );
	}
}

void LavaSwapChain::SetupFrameBuffers()
{
	m_frameBuffers.resize( m_swapChainImageViews.size() );

	for( size_t i = 0; i < m_swapChainImageViews.size(); i++ )
	{
		VkImageView attachments[] = {
			m_swapChainImageViews[i]
		};

		VkFramebufferCreateInfo framebufferInfo = {};
		framebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		framebufferInfo.renderPass = m_backBufferRenderPass;
		framebufferInfo.attachmentCount = 1;
		framebufferInfo.pAttachments = attachments;
		framebufferInfo.width = RenderContext::appRenderContext.extent.width;
		framebufferInfo.height = RenderContext::appRenderContext.extent.height;
		framebufferInfo.layers = 1;

		if( vkCreateFramebuffer( RenderContext::appRenderContext.logicalDevice, &framebufferInfo, nullptr, &m_frameBuffers[i] ) != VK_SUCCESS )
		{
			throw std::runtime_error( "failed to create framebuffer!" );
		}
	}
}

void LavaSwapChain::SetupCommandPool()
{
	VkCommandPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	poolInfo.queueFamilyIndex = RenderContext::appRenderContext.presentFamily;
	poolInfo.flags = 0; // Optional

	if( vkCreateCommandPool( RenderContext::appRenderContext.logicalDevice, &poolInfo, nullptr, &m_commandPool ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create command pool!" );
	}
}

void LavaSwapChain::SetupCommandBuffers()
{
	m_commandBuffers.resize( m_frameBuffers.size() );

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = m_commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = ( uint32_t) m_commandBuffers.size();

	if( vkAllocateCommandBuffers( RenderContext::appRenderContext.logicalDevice, &allocInfo, m_commandBuffers.data() ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to allocate command buffers!" );
	}

	for( size_t i = 0; i < m_commandBuffers.size(); i++ )
	{
		VkCommandBufferBeginInfo beginInfo = {};
		beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

		if( vkBeginCommandBuffer( m_commandBuffers[i], &beginInfo ) != VK_SUCCESS )
		{
			throw std::runtime_error( "failed to begin recording command buffer!" );
		}

		VkRenderPassBeginInfo renderPassInfo = {};
		renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
		renderPassInfo.renderPass = m_backBufferRenderPass;
		renderPassInfo.framebuffer = m_frameBuffers[i];
		renderPassInfo.renderArea.offset = { 0, 0 };
		renderPassInfo.renderArea.extent = m_swapChainSupport.currentExtent;

		VkClearValue clearColor = { 0.1f, 0.1f, 0.1f, 1.0f };
		renderPassInfo.clearValueCount = 1;
		renderPassInfo.pClearValues = &clearColor;

		vkCmdBeginRenderPass( m_commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE );

		// this needs to come from the scene...
		for( auto renderable : m_renderables )
		{
			renderable->Draw( m_commandBuffers[i] );
		}
		
		vkCmdEndRenderPass( m_commandBuffers[i] );

		if( vkEndCommandBuffer( m_commandBuffers[i] ) != VK_SUCCESS )
		{
			throw std::runtime_error( "failed to record command buffer!" );
		}
	}
	m_recreateCommandBuffers = false;
}

void LavaSwapChain::SetupSync()
{
	m_imageAvailableSemaphores.resize( MAX_FRAMES_IN_FLIGHT );
	m_renderFinishedSemaphores.resize( MAX_FRAMES_IN_FLIGHT );
	m_inFlightFences.resize( MAX_FRAMES_IN_FLIGHT );

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for( size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; i++ )
	{
		if( vkCreateSemaphore( RenderContext::appRenderContext.logicalDevice, &semaphoreInfo, nullptr, &m_imageAvailableSemaphores[i] ) != VK_SUCCESS ||
			vkCreateSemaphore( RenderContext::appRenderContext.logicalDevice, &semaphoreInfo, nullptr, &m_renderFinishedSemaphores[i] ) != VK_SUCCESS ||
			vkCreateFence( RenderContext::appRenderContext.logicalDevice, &fenceInfo, nullptr, &m_inFlightFences[i] ) != VK_SUCCESS )
		{
			throw std::runtime_error( "failed to create synchronization objects for a frame!" );
		}
	}

}

void LavaSwapChain::AddRenderable( LavaRenderable* renderable )
{
	renderable->Finalize( m_backBufferRenderPass );
	m_renderables.push_back( renderable );
	m_recreateCommandBuffers = true;
}

void LavaSwapChain::RemoveRenderable( LavaRenderable* renderable )
{
	auto place = std::find(m_renderables.begin(), m_renderables.end(), renderable);
	if( place != m_renderables.end() )
	{
		m_renderables.erase( place );
		m_recreateCommandBuffers = true;
	}
}

void LavaSwapChain::Render()
{
	if( m_recreateCommandBuffers )
	{
		SetupCommandBuffers();
	}

	vkWaitForFences( RenderContext::appRenderContext.logicalDevice, 1, &m_inFlightFences[m_currentFrame], VK_TRUE, (std::numeric_limits<uint64_t>::max)() );

	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR( RenderContext::appRenderContext.logicalDevice, m_swapChain, (std::numeric_limits<uint64_t>::max)(), m_imageAvailableSemaphores[m_currentFrame], VK_NULL_HANDLE, &imageIndex );

	if( result == VK_ERROR_OUT_OF_DATE_KHR )
	{
		Recreate();
		return;
	}
	else if( result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR )
	{
		throw std::runtime_error( "failed to acquire swap chain image!" );
	}

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;

	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &m_commandBuffers[imageIndex];

	VkSemaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;
	
	vkResetFences( RenderContext::appRenderContext.logicalDevice, 1, &m_inFlightFences[m_currentFrame] );

	if( vkQueueSubmit( RenderContext::appRenderContext.graphicsQueue, 1, &submitInfo, m_inFlightFences[m_currentFrame] ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to submit draw command buffer!" );
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;

	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = { m_swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;

	presentInfo.pImageIndices = &imageIndex;

	vkQueuePresentKHR( RenderContext::appRenderContext.presentQueue, &presentInfo );

	m_currentFrame = ( m_currentFrame + 1 ) % MAX_FRAMES_IN_FLIGHT;
}

bool LavaSwapChain::DeviceSupportsExtensions( VkPhysicalDevice physicalDevice )
{
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties( physicalDevice, nullptr, &extensionCount, nullptr );

	std::vector<VkExtensionProperties> availableExtensions( extensionCount );
	vkEnumerateDeviceExtensionProperties( physicalDevice, nullptr, &extensionCount, availableExtensions.data() );

	std::set<std::string> requiredExtensions( SwapChainConstants::EXTENSIONS.begin(), SwapChainConstants::EXTENSIONS.end() );

	for( const auto& extension : availableExtensions )
	{
		requiredExtensions.erase( extension.extensionName );
	}

	return requiredExtensions.empty();
}

bool LavaSwapChain::QueryDeviceSwapChainSupport( RenderContext::RenderContext& context, SwapChainSupport& support )
{
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR( context.physicalDevice, context.surface, &support.capabilities );
	
	if( support.capabilities.currentExtent.width != ( std::numeric_limits<uint32_t>::max )() )
	{
		support.currentExtent = support.capabilities.currentExtent;
	}
	else
	{
		int width, height;
		glfwGetFramebufferSize( context.window, &width, &height );

		support.currentExtent = {
			static_cast<uint32_t>( width ),
			static_cast<uint32_t>( height )
		};
	}

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR( context.physicalDevice, context.surface, &formatCount, nullptr );

	if( formatCount != 0 )
	{
		support.formats.resize( formatCount );
		vkGetPhysicalDeviceSurfaceFormatsKHR( context.physicalDevice, context.surface, &formatCount, support.formats.data() );
		
		if( !support.formats.empty() )
		{
			support.currentFormat = support.formats[0];
			for( const auto& availableFormat : support.formats )
			{
				if( availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR )
				{
					support.currentFormat = availableFormat;
				}
			}
		}
	}

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR( context.physicalDevice, context.surface, &presentModeCount, nullptr );

	if( presentModeCount != 0 )
	{
		support.presentModes.resize( presentModeCount );
		vkGetPhysicalDeviceSurfacePresentModesKHR( context.physicalDevice, context.surface, &presentModeCount, support.presentModes.data() );
		
		if( std::find( support.presentModes.begin(), support.presentModes.end(), VK_PRESENT_MODE_MAILBOX_KHR ) != support.presentModes.end() )
		{
			support.currentPresentMode = VK_PRESENT_MODE_MAILBOX_KHR;
		}
		else
		{
			support.currentPresentMode = VK_PRESENT_MODE_FIFO_KHR;
		}

	}

	return !support.formats.empty() && !support.presentModes.empty();
}

