#pragma once
#include "results.h"

namespace ImageUtils
{
	Lava::Result<VkImageView> CreateImageViewFromImage( const VkImage* image, const VkFormat* imageFormat, VkDevice device );
}