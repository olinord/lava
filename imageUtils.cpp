#include "pch.h"
#include "imageUtils.h"
#include "results.h"

namespace ImageUtils
{
	Lava::Result<VkImageView> CreateImageViewFromImage( const VkImage* image, const VkFormat* imageFormat, VkDevice device )
	{
		VkImageView imageView;
		VkImageViewCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		createInfo.image = *image;
		createInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		createInfo.format = *imageFormat;
		createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
		createInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		createInfo.subresourceRange.baseMipLevel = 0;
		createInfo.subresourceRange.levelCount = 1;
		createInfo.subresourceRange.baseArrayLayer = 0;
		createInfo.subresourceRange.layerCount = 1;

		return { vkCreateImageView( device, &createInfo, nullptr, &imageView ) == VK_SUCCESS, imageView };
	}
}