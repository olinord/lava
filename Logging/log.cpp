
#include "pch.h"
#include "log.h"

#define print_formatted_string(prefix, message, postfix) \
    va_list args;\
	va_start( args, message );\
	size_t len = std::vsnprintf( NULL, 0, message.c_str(), args );\
	va_end( args );\
	std::vector<char> vec( len + 1 );\
	va_start( args, message );\
	std::vsnprintf( &vec[0], len + 1, message.c_str(), args );\
	va_end( args );\
	std::cout << prefix << &vec[0] << postfix << std::endl;\

namespace Logging
{
	void Debug( std::string message, ... )
	{
#ifndef NDEBUG
		print_formatted_string("DEBUG: ", message, "")
#endif
	}

	void Info( std::string message, ... )
	{
		print_formatted_string( "INFO: ", message, "" )
	}
	
	void Warn( std::string message, ... )
	{
		print_formatted_string( "WARN: ", message, "" )
	}
	
	void Error( std::string message, ... )
	{
		print_formatted_string( "ERROR: ", message, "" )
	}
}