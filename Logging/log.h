#pragma once
#include "..//dllDeclspecs.h"

namespace Logging
{
	void EXPORT Debug( std::string message, ... );
	void EXPORT Info( std::string message, ... );
	void EXPORT Warn( std::string message, ... );
	void EXPORT Error( std::string message, ... );
}