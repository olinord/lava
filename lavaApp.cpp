#include "pch.h"
#include "lavaApp.h"
#include <cassert>

LavaApp::LavaApp():
	m_window( nullptr ),
	m_device( nullptr ),
	m_instance( VK_NULL_HANDLE ),
	m_resized( false )
{}

LavaApp::~LavaApp()
{
	CleanUp();
}

void LavaApp::Setup( uint32_t width, uint32_t height, const char* windowName )
{
	glfwInit();

	glfwWindowHint( GLFW_CLIENT_API, GLFW_NO_API );
	m_window = glfwCreateWindow( width, height, windowName, nullptr, nullptr );
	
	m_lavaValidator = LavaValidator();
	m_lavaValidator.AssertLayers();

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = windowName;
	appInfo.applicationVersion = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.pEngineName = "Lava";
	appInfo.engineVersion = VK_MAKE_VERSION( 1, 0, 0 );
	appInfo.apiVersion = VK_API_VERSION_1_1;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions( &glfwExtensionCount );

	m_extensions = std::vector<const char*>( glfwExtensions, glfwExtensions + glfwExtensionCount );

	m_lavaValidator.Attach< VkInstanceCreateInfo >( createInfo, m_extensions );

	if( vkCreateInstance( &createInfo, nullptr, &m_instance ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create instance!" );
	}
	m_lavaValidator.SetupMessenger( m_instance );

	m_device = new LavaDevice( m_instance, m_window, m_lavaValidator );
}

void LavaApp::CleanUp()
{
	m_device->CleanUp( m_instance );
	delete m_device;
	m_lavaValidator.CleanUp( m_instance );

	vkDestroyInstance( m_instance, nullptr );
	glfwDestroyWindow( m_window );
	glfwTerminate();
}

void LavaApp::AddRenderable( LavaRenderable* renderable )
{
	m_device->GetSwapChain()->AddRenderable( renderable );
}

void LavaApp::RemoveRenderable( LavaRenderable* renderable )
{
	m_device->GetSwapChain()->RemoveRenderable( renderable );
}

bool LavaApp::ShouldClose()
{
	assert( m_window != nullptr );

	return glfwWindowShouldClose( m_window );		
}

void LavaApp::PollEvents()
{
	glfwPollEvents();
}

void LavaApp::Render()
{
	m_device->Render();
}