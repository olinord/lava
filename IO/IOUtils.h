#pragma once

namespace IOUtils
{
	const std::vector<char> ReadShaderFile( const std::string& file );
	const std::vector<char> ReadFile( const std::string& file );
}