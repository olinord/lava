#include "pch.h"
#include "IOUtils.h"

namespace IOUtils
{
	const std::vector<char> ReadShaderFile( const std::string& filePath )
	{
		std::string resolvedPath = "Shaders/" + filePath + ".spv";

		return ReadFile( resolvedPath);
	}

	const std::vector<char> ReadFile( const std::string& filePath )
	{
		Logging::Info( "Reading file %s", filePath.c_str() );
		std::ifstream file( filePath, std::ios::ate | std::ios::binary );

		if( !file.is_open() )
		{
			Logging::Error( "Could not find file %s", filePath.c_str() );
			return std::vector<char>();
		}

		size_t fileSize = (size_t) file.tellg();
		std::vector<char> buffer( fileSize );

		file.seekg( 0 );
		file.read( buffer.data(), fileSize );

		file.close();

		return buffer;
	}
}