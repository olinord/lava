#include "pch.h"
#include "lavaBuffer.h"

LavaBuffer::LavaBuffer( uint32_t stride, std::vector<VkVertexInputAttributeDescription>& descriptions ) :
	m_attributeDescriptions( descriptions ),
	m_buffer( new VkBuffer() ),
	m_bufferMemory( new VkDeviceMemory() ),
	m_stride( stride ),
	m_elementCount( 0 )
{
	m_bindingDescription = {};
	m_bindingDescription.binding = 0;
	m_bindingDescription.stride = m_stride;
	m_bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

}

LavaBuffer::~LavaBuffer()
{

}

void LavaBuffer::SetData( void* bufferData, size_t dataSize )
{
	if( dataSize != m_elementCount * m_stride )
	{
		vkDestroyBuffer( RenderContext::appRenderContext.logicalDevice, *m_buffer, nullptr );
		vkFreeMemory( RenderContext::appRenderContext.logicalDevice, *m_bufferMemory, nullptr );
		CreateBuffer( dataSize );
		ReserveMemory();
	}

	PopulateMemory( bufferData, dataSize );
	m_elementCount = uint32_t( dataSize ) / m_stride;
};

void LavaBuffer::PopulateMemory( void* bufferData, size_t size )
{
	void* data;
	vkMapMemory( RenderContext::appRenderContext.logicalDevice, *m_bufferMemory, 0, size, 0, &data );
	memcpy( data, bufferData, size );
	vkUnmapMemory( RenderContext::appRenderContext.logicalDevice, *m_bufferMemory );
}

void LavaBuffer::Cleanup()
{
	if( m_buffer != nullptr )
	{
		vkDestroyBuffer( RenderContext::appRenderContext.logicalDevice, *m_buffer, nullptr );
		m_buffer = nullptr;
	}

	if( m_bufferMemory != nullptr )
	{
		vkFreeMemory( RenderContext::appRenderContext.logicalDevice, *m_bufferMemory, nullptr );
		m_bufferMemory = nullptr;
	}
	m_attributeDescriptions.clear();
}

VkBufferCreateInfo LavaBuffer::CreateBuffer( size_t size )
{
	VkBufferCreateInfo bufferInfo = {};
	bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferInfo.size = size;
	bufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
	bufferInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	
	if( vkCreateBuffer( RenderContext::appRenderContext.logicalDevice, &bufferInfo, nullptr, m_buffer ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create vertex buffer!" );
	}
	return bufferInfo;
}

void LavaBuffer::ReserveMemory()
{
	VkMemoryRequirements memRequirements;
	vkGetBufferMemoryRequirements( RenderContext::appRenderContext.logicalDevice, *m_buffer, &memRequirements );
	VkMemoryAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = FindMemoryType( memRequirements.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT );
	
	if( vkAllocateMemory( RenderContext::appRenderContext.logicalDevice, &allocInfo, nullptr, m_bufferMemory ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to allocate vertex buffer memory!" );
	}

	vkBindBufferMemory( RenderContext::appRenderContext.logicalDevice, *m_buffer, *m_bufferMemory, 0 );
}

uint32_t LavaBuffer::FindMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags properties )
{
	VkPhysicalDeviceMemoryProperties memProperties;
	vkGetPhysicalDeviceMemoryProperties( RenderContext::appRenderContext.physicalDevice, &memProperties );

	for( uint32_t i = 0; i < memProperties.memoryTypeCount; i++ )
	{
		if( ( typeFilter & ( 1 << i ) ) && ( memProperties.memoryTypes[i].propertyFlags & properties ) == properties )
		{
			return i;
		}
	}

	throw std::runtime_error( "failed to find suitable memory type!" );
}

VkPipelineVertexInputStateCreateInfo LavaBuffer::GetVertexInputCreateInfo()
{
	VkPipelineVertexInputStateCreateInfo vertexInputInfo = {};
	vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;

	vertexInputInfo.vertexBindingDescriptionCount = 1;
	vertexInputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>( m_attributeDescriptions.size() );
	vertexInputInfo.pVertexBindingDescriptions = &m_bindingDescription;

	vertexInputInfo.pVertexAttributeDescriptions = m_attributeDescriptions.data();
	return vertexInputInfo;
}

void LavaBuffer::Bind( VkCommandBuffer& commandBuffer )
{
	VkBuffer vertexBuffers[] = { *m_buffer };
	VkDeviceSize offsets[] = { 0 };
	vkCmdBindVertexBuffers( commandBuffer, 0, 1, vertexBuffers, offsets );
}