#pragma once

#include "../dllDeclspecs.h"
#include "../results.h"
#include "../lavaRenderContext.h"
#include "lavaBuffer.h"

/*
This class contains everything about how to render a thing
*/
class LavaRenderable
{
public:
	EXPORT LavaRenderable( );
	EXPORT ~LavaRenderable();

	EXPORT void CleanUp();

	EXPORT void Draw( VkCommandBuffer& commandBuffer );
	
	EXPORT void SetVertexShader( const std::string shaderPath );
	EXPORT void SetTessellationControlShader( const std::string shaderPath );
	EXPORT void SetTessellationEvaluationShader( const std::string shaderPath );
	EXPORT void SetGeometryShader( const std::string shaderPath );
	EXPORT void SetPixelShader( const std::string shaderPath );
	EXPORT void SetComputeShader( const std::string shaderPath );

	// NVIDIA SPECIFIC SHADERS
	EXPORT void SetMeshShader( const std::string shaderPath );
	EXPORT void SetTaskShader( const std::string shaderPath );
	EXPORT void SetRayGenerationShader( const std::string shaderPath );
	EXPORT void SetRayIntersectionShader( const std::string shaderPath );
	EXPORT void SetRayAnyHitShader( const std::string shaderPath );
	EXPORT void SetRayClosestHitShader( const std::string shaderPath );
	EXPORT void SetRayMissShader( const std::string shaderPath );
	EXPORT void SetRayCallableShader( const std::string shaderPath );

	EXPORT void SetMesh( const std::string meshPath );

	EXPORT void SetVertexBuffer( LavaBuffer* vertexBuffer );
	
	EXPORT void SetVertexBuffer( void* data, uint32_t stride, size_t count, std::vector<VkVertexInputAttributeDescription> attributes );
	EXPORT void UpdateVertexBuffer( void* data, size_t count );
	EXPORT void Finalize( VkRenderPass& renderPass );

private:
	void AddShader( const std::string shaderPath, VkShaderStageFlagBits stageFlag );
	bool CreateShaderModule( const std::string shaderPath, VkShaderModule& module );

	std::vector< VkPipelineShaderStageCreateInfo > m_stages;
	std::map<VkShaderStageFlagBits, std::string > m_shaders;
	std::vector< VkShaderModule > m_shaderModules;

	LavaBuffer* m_buffer;

	VkPipelineLayout m_pipelineLayout;
	VkPipeline m_graphicsPipeline;

};