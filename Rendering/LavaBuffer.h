#pragma once

#include"../dllDeclspecs.h"
#include"../lavaRenderContext.h"


class LavaBuffer
{
public:
	EXPORT LavaBuffer( uint32_t stride, std::vector<VkVertexInputAttributeDescription>& descriptions );
	EXPORT ~LavaBuffer();

	EXPORT void Cleanup();
	EXPORT void SetData( void* data, size_t dataSize );

	EXPORT void Bind( VkCommandBuffer& commandBuffer );
	EXPORT VkPipelineVertexInputStateCreateInfo GetVertexInputCreateInfo();

	EXPORT uint32_t GetElementCount()
	{
		return m_elementCount;
	}

	EXPORT uint32_t GetStride()
	{
		return m_stride;
	}

private:
	VkBufferCreateInfo CreateBuffer( size_t size );
	void ReserveMemory();
	void PopulateMemory( void* bufferData, size_t size );
	uint32_t FindMemoryType( uint32_t typeFilter, VkMemoryPropertyFlags properties );


	VkVertexInputBindingDescription m_bindingDescription;
	std::vector<VkVertexInputAttributeDescription> m_attributeDescriptions;
	VkBuffer* m_buffer;
	VkDeviceMemory* m_bufferMemory;

	uint32_t m_stride;
	uint32_t m_elementCount;
};
