#include "pch.h"
#include "lavaRenderingUtils.h"
#include "../lavaDevice.h"
#include "../IO/IOUtils.h"

namespace LavaRenderingUtils
{
	bool MakeShaderModule( const std::vector<char>& code, VkShaderModule& module )
	{
		VkShaderModuleCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
		createInfo.codeSize = code.size();
		createInfo.pCode = reinterpret_cast<const uint32_t*>( code.data() );
		
		return vkCreateShaderModule( RenderContext::appRenderContext.logicalDevice, &createInfo, nullptr, &module) == VK_SUCCESS;
	}

}