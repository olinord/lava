#pragma once
#include"../dllDeclspecs.h"
#include"../lavaRenderContext.h"

namespace LavaRenderingUtils
{
	bool MakeShaderModule( const std::vector<char>& code, VkShaderModule& module );
}