#include "pch.h"
#include "lavaRenderable.h"
#include "lavaRenderingUtils.h"
#include "..//IO/IOUtils.h"


LavaRenderable::LavaRenderable( ): 
	m_graphicsPipeline( 0 ),
	m_pipelineLayout( 0 ),
	m_buffer( nullptr )
{
}

LavaRenderable::~LavaRenderable()
{

}

void LavaRenderable::SetVertexBuffer( LavaBuffer* vertexBuffer )
{
	m_buffer = vertexBuffer;
}

void LavaRenderable::SetVertexBuffer( void* data, uint32_t stride, size_t count, std::vector<VkVertexInputAttributeDescription> attributes )
{
	if( m_buffer != nullptr )
	{
		m_buffer->Cleanup();
		delete m_buffer;
	}
	m_buffer = new LavaBuffer( stride, attributes );
	m_buffer->SetData( data, stride * count );
}

void LavaRenderable::UpdateVertexBuffer( void* data, size_t count )
{
	m_buffer->SetData( data, count * m_buffer->GetStride() );
}

void LavaRenderable::Draw( VkCommandBuffer& commandBuffer )
{
	vkCmdBindPipeline( commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, m_graphicsPipeline );

	if( m_buffer != nullptr )
	{
		auto elements = m_buffer->GetElementCount();
		m_buffer->Bind( commandBuffer );
		vkCmdDraw( commandBuffer, elements, elements / 3, 0, 0 );
	}
}

void LavaRenderable::CleanUp()
{
	if( m_buffer != nullptr )
	{
		m_buffer->Cleanup();
		delete m_buffer;
	}
	vkDestroyPipeline( RenderContext::appRenderContext.logicalDevice, m_graphicsPipeline, nullptr );
	vkDestroyPipelineLayout( RenderContext::appRenderContext.logicalDevice, m_pipelineLayout, nullptr );
	m_graphicsPipeline = 0;
	m_pipelineLayout = 0;
	m_stages.clear();
}

void LavaRenderable::AddShader( const std::string shaderPath, VkShaderStageFlagBits stageFlag )
{
	m_shaders[stageFlag] = shaderPath;
}

bool LavaRenderable::CreateShaderModule( const std::string shaderPath, VkShaderModule& module )
{
	auto code = IOUtils::ReadShaderFile( shaderPath );
	if( code.empty() )
	{
		Logging::Error( "Could not load shader %s.", shaderPath.c_str() );
		return false;
	}
	;
	if( !LavaRenderingUtils::MakeShaderModule( code, module ) )
	{
		Logging::Error( "Could not create shader %s.", shaderPath.c_str() );
		return false;
	}
	return true;
}

void LavaRenderable::SetVertexShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_VERTEX_BIT );
}

void LavaRenderable::SetTessellationControlShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT );
}

void LavaRenderable::SetTessellationEvaluationShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT );
}

void LavaRenderable::SetGeometryShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_GEOMETRY_BIT );
}

void LavaRenderable::SetPixelShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_FRAGMENT_BIT );
}

void LavaRenderable::SetComputeShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_COMPUTE_BIT );
}

// START NVIDIA SPECIFIC SHADERS
void LavaRenderable::SetMeshShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_MESH_BIT_NV );
}

void LavaRenderable::SetTaskShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_TASK_BIT_NV );
}

void LavaRenderable::SetRayGenerationShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_RAYGEN_BIT_NV );
}

void LavaRenderable::SetRayIntersectionShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_INTERSECTION_BIT_NV );
}

void LavaRenderable::SetRayAnyHitShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_ANY_HIT_BIT_NV);
}

void LavaRenderable::SetRayClosestHitShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_CLOSEST_HIT_BIT_NV );
}

void LavaRenderable::SetRayMissShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_MISS_BIT_NV );
}

void LavaRenderable::SetRayCallableShader( const std::string shaderPath )
{
	AddShader( shaderPath, VK_SHADER_STAGE_CALLABLE_BIT_NV );
}
// END NVIDIA SPECIFIC SHADERS

void LavaRenderable::SetMesh( const std::string meshPath )
{

}

void LavaRenderable::Finalize( VkRenderPass& renderPass )
{
	std::vector<VkShaderModule> modules;
	for( auto it : m_shaders )
	{
		VkShaderModule module;
		if( !CreateShaderModule( it.second, module ) )
		{
			Logging::Error( "LavaRenderable not finalized due to errors" );
			return;
		}
		VkPipelineShaderStageCreateInfo stageInfo;
		stageInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
		stageInfo.stage = it.first;
		stageInfo.module = module;
		stageInfo.flags = 0;
		stageInfo.pName = "main";
		stageInfo.pNext = nullptr;
		stageInfo.pSpecializationInfo = nullptr;
		m_stages.push_back( stageInfo );
		m_shaderModules.push_back( module );
	}

	VkPipelineVertexInputStateCreateInfo vertexInputInfo;
	
	if( m_buffer == nullptr )
	{
		vertexInputInfo = {};
		vertexInputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
		vertexInputInfo.vertexBindingDescriptionCount = 0;
		vertexInputInfo.vertexAttributeDescriptionCount = 0;
	}
	else
	{
		vertexInputInfo = m_buffer->GetVertexInputCreateInfo();
	}

	VkPipelineInputAssemblyStateCreateInfo inputAssembly = {};
	inputAssembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	inputAssembly.primitiveRestartEnable = VK_FALSE;
	
	VkViewport viewport = {};
	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width = ( float) RenderContext::appRenderContext.extent.width;
	viewport.height = ( float) RenderContext::appRenderContext.extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	VkRect2D scissor = {};
	scissor.offset = { 0, 0 };
	scissor.extent = RenderContext::appRenderContext.extent;

	VkPipelineViewportStateCreateInfo viewportState = {};
	viewportState.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewportState.viewportCount = 1;
	viewportState.pViewports = &viewport;
	viewportState.scissorCount = 1;
	viewportState.pScissors = &scissor;

	VkPipelineRasterizationStateCreateInfo rasterizer = {};
	rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterizer.depthClampEnable = VK_FALSE;
	rasterizer.rasterizerDiscardEnable = VK_FALSE;
	rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
	rasterizer.lineWidth = 1.0f;
	rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
	rasterizer.frontFace = VK_FRONT_FACE_CLOCKWISE;
	rasterizer.depthBiasEnable = VK_FALSE;

	VkPipelineMultisampleStateCreateInfo multisampling = {};
	multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisampling.sampleShadingEnable = VK_FALSE;
	multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;

	VkPipelineColorBlendAttachmentState colorBlendAttachment = {};
	colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
	colorBlendAttachment.blendEnable = VK_FALSE;

	VkPipelineColorBlendStateCreateInfo colorBlending = {};
	colorBlending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	colorBlending.logicOpEnable = VK_FALSE;
	colorBlending.logicOp = VK_LOGIC_OP_COPY;
	colorBlending.attachmentCount = 1;
	colorBlending.pAttachments = &colorBlendAttachment;
	colorBlending.blendConstants[0] = 0.0f;
	colorBlending.blendConstants[1] = 0.0f;
	colorBlending.blendConstants[2] = 0.0f;
	colorBlending.blendConstants[3] = 0.0f;

	VkPipelineLayoutCreateInfo pipelineLayoutInfo = {};
	pipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	pipelineLayoutInfo.setLayoutCount = 0;
	pipelineLayoutInfo.pushConstantRangeCount = 0;

	if( vkCreatePipelineLayout( RenderContext::appRenderContext.logicalDevice, &pipelineLayoutInfo, nullptr, &m_pipelineLayout ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create pipeline layout!" );
	}

	VkGraphicsPipelineCreateInfo pipelineInfo = {};
	pipelineInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	pipelineInfo.stageCount = static_cast<uint32_t>( m_stages.size() );
	pipelineInfo.pStages = m_stages.data();
	pipelineInfo.pVertexInputState = &vertexInputInfo;
	pipelineInfo.pInputAssemblyState = &inputAssembly;
	pipelineInfo.pViewportState = &viewportState;
	pipelineInfo.pRasterizationState = &rasterizer;
	pipelineInfo.pMultisampleState = &multisampling;
	pipelineInfo.pColorBlendState = &colorBlending;
	pipelineInfo.layout = m_pipelineLayout;
	pipelineInfo.renderPass = renderPass;
	pipelineInfo.subpass = 0;
	pipelineInfo.basePipelineHandle = VK_NULL_HANDLE;

	auto result = vkCreateGraphicsPipelines( RenderContext::appRenderContext.logicalDevice, VK_NULL_HANDLE, 1, &pipelineInfo, nullptr, &m_graphicsPipeline );
	if( result != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create graphics pipeline!" );
	}

	for( auto shaderModule : m_shaderModules )
	{
		vkDestroyShaderModule( RenderContext::appRenderContext.logicalDevice, shaderModule, nullptr );
	}
	m_shaderModules.clear();
}

