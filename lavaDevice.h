#pragma once
#include "dllDeclspecs.h"
#include "lavaValidator.h"
#include "Rendering/lavaRenderingUtils.h"
#include "lavaSwapChain.h"

class LavaDevice
{
public:
	EXPORT LavaDevice( VkInstance& instance, GLFWwindow* window, LavaValidator& validator );
	EXPORT ~LavaDevice();
	EXPORT void CleanUp( VkInstance& instance );

	EXPORT bool GetQueueFamilyIndex( uint32_t bits, VkPhysicalDevice device, uint32_t &index );
	EXPORT bool GetPresentFamilyIndex( VkPhysicalDevice device, uint32_t& index );

	EXPORT void Render();

	EXPORT LavaSwapChain* GetSwapChain()
	{
		return m_swapchain;
	};
	
private:
	bool IsDeviceSuitable( VkPhysicalDevice device );
	void SetupPhysicalDevice( VkInstance& instance );
	void SetupLogicalDevice( VkInstance& instance, LavaValidator& validator );
	void SetupSurface( VkInstance& instance, GLFWwindow* window );

	VkPhysicalDevice m_physicalDevice;
	VkDevice m_logicalDevice;

	VkSurfaceKHR m_surface;

	VkQueue m_graphicsQueue;
	VkQueue m_presentQueue;

	LavaSwapChain* m_swapchain;

	uint32_t m_presentFamily;
	uint32_t m_graphicsFamily;
};