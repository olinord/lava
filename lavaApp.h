#pragma once
#include "dllDeclspecs.h"
#include "lavaDevice.h"
#include "lavaValidator.h"
#include "Rendering/lavaRenderable.h"

class LavaApp
{
public:
	EXPORT LavaApp();
	EXPORT ~LavaApp();
	EXPORT void Setup( uint32_t width, uint32_t height, const char* windowName );
	
	EXPORT bool ShouldClose();
	EXPORT void PollEvents();
	EXPORT void Render();

	EXPORT void AddRenderable( LavaRenderable* renderable );
	EXPORT void RemoveRenderable( LavaRenderable* renderable );

private:
	void CleanUp();

	GLFWwindow* m_window;
	VkInstance m_instance;
	LavaValidator m_lavaValidator;

	LavaDevice* m_device;
	std::vector<const char*> m_extensions;
	bool m_resized;
};