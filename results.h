#pragma once
#include "dllDeclspecs.h"

namespace Lava
{
	template<typename T>
	struct Result
	{
		bool ok;
		T value;
	};
}
