#include "pch.h"
#include "lavaDevice.h"
#include "lavaSwapChain.h"
#include "lavaRenderContext.h"

LavaDevice::LavaDevice( VkInstance& instance, GLFWwindow* window, LavaValidator& validator ):
	m_physicalDevice( VK_NULL_HANDLE ),
	m_swapchain( nullptr )
{
	RenderContext::appRenderContext.window = window;

	SetupSurface( instance, window );

	SetupPhysicalDevice( instance );
	SetupLogicalDevice( instance, validator );
	m_swapchain = new LavaSwapChain( );	
}

LavaDevice::~LavaDevice()
{
}

void LavaDevice::CleanUp( VkInstance& instance )
{
	vkDeviceWaitIdle( m_logicalDevice );

	m_swapchain->CleanUp();
	vkDestroySurfaceKHR( instance, m_surface, nullptr );
	vkDestroyDevice( m_logicalDevice, nullptr );
}

void LavaDevice::SetupPhysicalDevice( VkInstance& instance )
{
	uint32_t deviceCount = 0;
	vkEnumeratePhysicalDevices( instance, &deviceCount, nullptr );

	if( deviceCount == 0 )
	{
		throw std::runtime_error( "Failed to find GPUs with Vulkan support!" );
	}

	std::vector<VkPhysicalDevice> devices( deviceCount );
	vkEnumeratePhysicalDevices( instance, &deviceCount, devices.data() );

	for( const auto& device : devices )
	{
		if( IsDeviceSuitable( device ) )
		{
			m_physicalDevice = device;
			break;
		}
	}

	if( m_physicalDevice == VK_NULL_HANDLE )
	{
		throw std::runtime_error( "failed to find a suitable GPU!" );
	}

	RenderContext::appRenderContext.physicalDevice = m_physicalDevice;
}

void LavaDevice::SetupLogicalDevice( VkInstance& instance, LavaValidator& validator )
{
	GetQueueFamilyIndex( VK_QUEUE_GRAPHICS_BIT, m_physicalDevice, m_graphicsFamily );
	GetPresentFamilyIndex( m_physicalDevice, m_presentFamily );

	std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = { m_graphicsFamily, m_presentFamily };

	float queuePriority = 1.0f;
	for( uint32_t queueFamily : uniqueQueueFamilies )
	{
		VkDeviceQueueCreateInfo queueCreateInfo = {};
		queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo.queueFamilyIndex = queueFamily;
		queueCreateInfo.queueCount = 1;
		queueCreateInfo.pQueuePriorities = &queuePriority;
		queueCreateInfos.push_back( queueCreateInfo );
	}

	VkPhysicalDeviceFeatures deviceFeatures = {};

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;

	createInfo.queueCreateInfoCount = static_cast<uint32_t>( queueCreateInfos.size() );
	createInfo.pQueueCreateInfos = queueCreateInfos.data();

	createInfo.pEnabledFeatures = &deviceFeatures;
	
	createInfo.enabledExtensionCount = static_cast<uint32_t>( SwapChainConstants::EXTENSIONS.size() );
	createInfo.ppEnabledExtensionNames = SwapChainConstants::EXTENSIONS.data();

	validator.AttachToValidationLayer< VkDeviceCreateInfo >( createInfo );
	   
	if( vkCreateDevice( m_physicalDevice, &createInfo, nullptr, &m_logicalDevice ) != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create logical device!" );
	}

	vkGetDeviceQueue( m_logicalDevice, m_graphicsFamily, 0, &m_graphicsQueue );
	vkGetDeviceQueue( m_logicalDevice, m_presentFamily, 0, &m_presentQueue );
	
	RenderContext::appRenderContext.logicalDevice = m_logicalDevice;
	RenderContext::appRenderContext.graphicsFamily = m_graphicsFamily;
	RenderContext::appRenderContext.presentFamily = m_presentFamily;
	RenderContext::appRenderContext.graphicsQueue = m_graphicsQueue;
	RenderContext::appRenderContext.presentQueue = m_presentQueue;
}

void LavaDevice::SetupSurface( VkInstance& instance, GLFWwindow* window )
{
	VkResult result = glfwCreateWindowSurface( instance, window, nullptr, &m_surface );
	if( result != VK_SUCCESS )
	{
		throw std::runtime_error( "failed to create window surface!" );
	}
	RenderContext::appRenderContext.surface = m_surface;
}

bool LavaDevice::IsDeviceSuitable( VkPhysicalDevice device )
{
	uint32_t tmp;

	bool swapChainSupport = false;
	if( LavaSwapChain::DeviceSupportsExtensions( device ) )
	{
		SwapChainSupport caps;
		RenderContext::RenderContext tmpContext = RenderContext::RenderContext( RenderContext::appRenderContext );
		tmpContext.physicalDevice = device;
		swapChainSupport = LavaSwapChain::QueryDeviceSwapChainSupport( tmpContext, caps );
	}

	return GetQueueFamilyIndex( VK_QUEUE_GRAPHICS_BIT, device, tmp ) && swapChainSupport;
}

bool LavaDevice::GetQueueFamilyIndex( uint32_t queuebits, VkPhysicalDevice device, uint32_t& index )
{
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, nullptr );

	std::vector<VkQueueFamilyProperties> queueFamilies( queueFamilyCount );
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, queueFamilies.data() );

	int i = 0;
	for( const auto& queueFamily : queueFamilies )
	{
		if( queueFamily.queueCount > 0 && queueFamily.queueFlags & queuebits )
		{
			index = i;
			return true;
		}

		i++;
	}
	return false;
}

bool LavaDevice::GetPresentFamilyIndex( VkPhysicalDevice device, uint32_t& index )
{
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, nullptr );

	std::vector<VkQueueFamilyProperties> queueFamilies( queueFamilyCount );
	vkGetPhysicalDeviceQueueFamilyProperties( device, &queueFamilyCount, queueFamilies.data() );

	int i = 0;
	bool found = false;
	for( const auto& queueFamily : queueFamilies )
	{
		VkBool32 presentSupport = false;
		vkGetPhysicalDeviceSurfaceSupportKHR( device, i, m_surface, &presentSupport );

		if( queueFamily.queueCount > 0 && presentSupport )
		{
			index = i;
			return true;
		}

		i++;
	}
	return false;
}

void LavaDevice::Render()
{
	m_swapchain->Render();
}