#pragma once
#include "pch.h"

namespace RenderContext
{
	struct RenderContext
	{
		GLFWwindow* window;
		VkExtent2D extent;
		VkPhysicalDevice physicalDevice;
		VkDevice logicalDevice;
		uint32_t presentFamily;
		uint32_t graphicsFamily;
		VkSurfaceKHR surface;
		VkQueue graphicsQueue;
		VkQueue presentQueue;
	};

	extern RenderContext appRenderContext;
}
